# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Locate the dmtcp external package.
#
# Defines:
#  DMTCP_FOUND
#  DMTCP_INCLUDE_DIR
#  DMTCP_INCLUDE_DIRS
#  DMTCP_<component>_FOUND
#
# The user can set DMTCP_ROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME dmtcp
   INCLUDE_SUFFIXES include INCLUDE_NAMES dmtcp.h
   COMPULSORY_COMPONENTS dmtcp )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( dmtcp DEFAULT_MSG DMTCP_INCLUDE_DIRS )
mark_as_advanced( DMTCP_FOUND DMTCP_INCLUDE_DIR DMTCP_INCLUDE_DIRS )
